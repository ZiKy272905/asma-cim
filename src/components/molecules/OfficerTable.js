import Trash from '../atoms/ButtonTrash'
import Pen from '../atoms/ButtonPen'

export default function OfficerTable(){
	return(
        <div className="flex justify-center items-center w-full">
          <table className="w-full text-xs text-left text-gray-500 dark:text-gray-400">
            <thead className="text-sm text-gray-500 bg-gray-100">
              <tr>
                <th scope="col" className="px-6 py-3">
                  ID
                </th>
                <th scope="col" className="px-6 py-3">
                  Nama Lengkap
                </th>
                <th scope="col" className="px-6 py-3">
                  Username
                </th>
                <th scope="col" className="px-6 py-3">
                  No. Telp
                </th>
                <th scope="col" className="px-6 py-3">
                  Level
                </th>
                <th scope="col" className="px-6 py-3">
                  
                </th>
              </tr>
            </thead>

            <tbody>
              <tr className="bg-white border-b">
	              <td className="px-6 py-4">1</td>
                <td className="px-6 py-4">Rudi Jayadi</td>
                <td className="px-6 py-4">Rudi</td>
                <td className="px-6 py-4">0812 3456 7890</td>
                <td className="px-6 py-4">Admin</td>
                <td className="flex space-x-4 px-6 py-4">
                  <Trash/>
                  <Pen/>
                </td>
              </tr>
              <tr className="bg-white border-b">
                <td className="px-6 py-4">2</td>
                <td className="px-6 py-4">Muhammad Irfan</td>
                <td className="px-6 py-4">Irfan</td>
                <td className="px-6 py-4">0812 3456 7890</td>
                <td className="px-6 py-4">Petugas</td>
                <td className="flex space-x-4 px-6 py-4">
                  <Trash/>
                  <Pen/>
                </td>
              </tr>
              <tr className="bg-white border-b">
                <td className="px-6 py-4">3</td>
                <td className="px-6 py-4">Adam Malik</td>
                <td className="px-6 py-4">Adam</td>
                <td className="px-6 py-4">0812 3456 7890</td>
                <td className="px-6 py-4">Petugas</td>
                <td className="flex space-x-4 px-6 py-4">
                  <Trash/>
                  <Pen/>
                </td>
              </tr>
              <tr className="bg-white border-b">
                <td className="px-6 py-4">4</td>
                <td className="px-6 py-4">Iqbal Ramdani</td>
                <td className="px-6 py-4">Iqbal</td>
                <td className="px-6 py-4">0812 3456 7890</td>
                <td className="px-6 py-4">Petugas</td>
                <td className="flex space-x-4 px-6 py-4">
                  <Trash/>
                  <Pen/>
                </td>
              </tr>
              <tr className="bg-white border-b">
                <td className="px-6 py-4">5</td>
                <td className="px-6 py-4">Novi Ismajayanti</td>
                <td className="px-6 py-4">Novi</td>
                <td className="px-6 py-4">0812 3456 7890</td>
                <td className="px-6 py-4">Admin</td>
                <td className="flex space-x-4 px-6 py-4">
                  <Trash/>
                  <Pen/>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
	)
}