import React from 'react';
import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  BarElement,
  Title,
  Tooltip,
  Legend,
} from 'chart.js';
import { Bar } from 'react-chartjs-2';

ChartJS.register(
  CategoryScale,
  LinearScale,
  BarElement,
  Title,
  Tooltip,
  Legend
);

const options = {
  responsive: true,
  plugins: {
    legend: {
      position: 'top',
    },
    title: {
      display: true,
      text: 'Tanggapan',
    },
  },
  animation: {
    duration: 2000, 
    easing: 'easeInOutQuad',
  },
};

const labels = ['January', 'February', 'March', 'April', 'May', 'June', 'July'];

export const data = {
  labels,
  datasets: [
    {
      label: 'Diterima',
      data: [50, 60, 49, 65, 40, 60, 79],
      backgroundColor: 'rgba(43, 100, 210, 1)',
    },
    {
      label: 'Ditolak',
      data: [20, 31, 40, 20, 15, 22, 50],
      backgroundColor: 'rgba(232, 45, 47, 1)',
    },
  ],
};

export default function VerticalBar() {
  return <Bar options={options} data={data} />;
}
