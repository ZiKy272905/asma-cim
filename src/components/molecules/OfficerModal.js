import Button from '../atoms/Button'
import ButtonCancel from '../atoms/ButtonCancel'
import Input from '../atoms/Input'
import Label from '../atoms/Label'
import Notif from './Notification'
import Check from '../atoms/icons/SquareCheck'
import { useState, useEffect } from 'react'

export default function OfficerModal({ show, setShow }) {
	const [submitted, setSubmitted] = useState(false);

  const notification = submitted && (
      <Notif
        setShow={setSubmitted}
        show={submitted}
        title="Notification"
        message="Successfully added CoA."
        icon={<Check/>}
      />
    );

	const content = show && (
			<div className="flex justify-center items-center z-0 fixed md:inset-0 h-modal md:h-full bg-[black] bg-opacity-[25%] backdrop-blur-sm z-20">
		        <div className="bg-white rounded-lg w-[35%] shadow-md">
		        	<div className="flex justify-start items-center border-b w-full px-6 py-4">
		        		<h1 className="text-md font-medium">Tambah Petugas</h1>
		        	</div>
		            <div className="flex flex-col justify-center items-center w-full h-full py-6">
		                <form className="flex flex-col w-full h-[80%] px-14 overflow-y-auto">
		                	<div className="flex flex-col justify-start w-full space-y-4 mb-8">
		                			<div className="flex flex-col w-full">
			                        <select className="w-full p-2 text-sm text-gray-900 rounded-md focus:outline-none focus:ring-0 border">
			                        	<option>Admin</option>
			                        	<option>Petugas</option>
			                        </select>
		                    	</div>
		                    	<div className="flex flex-col w-full">
			                        <Label text="Nama Lengkap"/>
			                        <Input
			                        	placeholder="Nama Lengkap"
			                        	type="text"
			                        />
		                    	</div>
		                    	<div className="flex flex-col w-full">
			                        <Label text="Username"/>
			                        <Input
			                        	placeholder="Username"
			                        	type="text"
			                        />
		                    	</div>
		                    	<div className="flex flex-col w-full">
			                        <Label text="Password"/>
			                        <Input
			                        	placeholder="Password"
			                        	type="text"
			                        />
		                    	</div>
		                    	<div className="flex flex-col w-full">
			                        <Label text="No. Telp"/>
			                        <Input
			                        	placeholder="0123456789"
			                        	type="number"
			                        />
		                    	</div>
		                	</div>
		                    <div className="flex justify-center items-center w-full space-x-12 px-4">
		                    	<ButtonCancel 
		                    		title="Cancel"
		                    		onClick={() => setShow(false)}
		                    	/>
		                    	<Button 
			                    	title="Submit" 
			                    	type="submit"
		                    	/>
		                    </div>
		                </form>
		            </div>
		        </div>
			</div>
		)

	return ([content, notification])
}