import Trash from '../atoms/ButtonTrash'
import Pen from '../atoms/ButtonPen'

export default function PeopleTable(){
	return(
        <div className="flex justify-center items-center w-full">
          <table className="w-full text-xs text-left text-gray-500 dark:text-gray-400">
            <thead className="text-sm text-gray-500 bg-gray-100">
              <tr>
                <th scope="col" className="px-6 py-3">
                  NIK
                </th>
                <th scope="col" className="px-6 py-3">
                  Nama Lengkap
                </th>
                <th scope="col" className="px-6 py-3">
                  Username
                </th>
                <th scope="col" className="px-6 py-3">
                  No. Telp
                </th>
              </tr>
            </thead>

            <tbody>
              <tr className="bg-white border-b">
	              <td className="px-6 py-4">1234 5678 9012 1314</td>
                <td className="px-6 py-4">Ali Akbar</td>
                <td className="px-6 py-4">Ali</td>
                <td className="px-6 py-4">0812 3456 7890</td>
              </tr>
              <tr className="bg-white border-b">
                <td className="px-6 py-4">1234 5678 9012 1314</td>
                <td className="px-6 py-4">Kevin Farhan</td>
                <td className="px-6 py-4">Kevin</td>
                <td className="px-6 py-4">0812 3456 7890</td>
              </tr>
              <tr className="bg-white border-b">
                <td className="px-6 py-4">1234 5678 9012 1314</td>
                <td className="px-6 py-4">Zidane Yazid</td>
                <td className="px-6 py-4">Zidane</td>
                <td className="px-6 py-4">0812 3456 7890</td>
              </tr>
              <tr className="bg-white border-b">
                <td className="px-6 py-4">1234 5678 9012 1314</td>
                <td className="px-6 py-4">Rifky Ramdhani</td>
                <td className="px-6 py-4">Rifky</td>
                <td className="px-6 py-4">0812 3456 7890</td>
              </tr>
              <tr className="bg-white border-b">
                <td className="px-6 py-4">1234 5678 9012 1314</td>
                <td className="px-6 py-4">Raihan Taufik</td>
                <td className="px-6 py-4">Raihan</td>
                <td className="px-6 py-4">0812 3456 7890</td>
              </tr>
            </tbody>
          </table>
        </div>
	)
}