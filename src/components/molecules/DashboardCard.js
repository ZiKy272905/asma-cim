export default function DashboardCard({
		title,
		icon,
		fill
	}) {
	return(
		<div className="flex flex-col justify-center items-center w-full space-y-2 p-4">
			<div className="flex justify-center items-center text-center w-full">
				<h1 className="text-lg font-semibold">{title}</h1>
			</div>

			<div className="flex justify-center items-center w-full space-x-2">
				<div className="flex justify-center items-center">
					{icon}
				</div>
				<div className="flex justify-center items-center">
					<h1 className="text-4xl font-medium">{fill}</h1>
				</div>
			</div>
		</div>
	)
}