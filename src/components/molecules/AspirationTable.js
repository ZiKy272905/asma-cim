import Accept from '../atoms/ButtonAccept'
import Reject from '../atoms/ButtonReject'

export default function AspirationTable(){
	return(
        <div className="flex justify-center items-center w-full">
          <table className="w-full text-xs text-left text-gray-500 dark:text-gray-400">
            <thead className="text-sm text-gray-500 bg-gray-100">
              <tr>
                <th scope="col" className="px-6 py-3">
                  ID
                </th>
                <th scope="col" className="px-6 py-3">
                  NIK
                </th>
                <th scope="col" className="px-6 py-3">
                  Aspirasi
                </th>
                <th scope="col" className="px-6 py-3">
                  Status
                </th>
                <th scope="col" className="px-6 py-3">
                  Judul Aspirasi
                </th>
                <th scope="col" className="px-6 py-3">
                  Asal Pelapor
                </th>
                <th scope="col" className="px-6 py-3">
                  
                </th>
              </tr>
            </thead>

            <tbody>
              <tr className="bg-white border-b">
	            <td className="px-6 py-4">1</td>
                <td className="px-6 py-4">1234 5678 91011 1213</td>
                <td className="px-6 py-4">Perbaikan Fasilitas Umum</td>
                <td className="px-6 py-4 text-green-500">Diterima</td>
                <td className="px-6 py-4">Perbaikan</td>
                <td className="px-6 py-4">Cimahi</td>
                <td className="flex space-x-4 px-6 py-4">
                  <Accept/>
                  <Reject/>
                </td>
              </tr>
              <tr className="bg-white border-b">
	            <td className="px-6 py-4">2</td>
                <td className="px-6 py-4">1234 5678 91011 1213</td>
                <td className="px-6 py-4">Pelayanan Perawatan Gratis</td>
                <td className="px-6 py-4 text-red-500">Ditolak</td>
                <td className="px-6 py-4">Pelayanan</td>
                <td className="px-6 py-4">Cimahi</td>
                <td className="flex space-x-4 px-6 py-4">
                  <Accept/>
                  <Reject/>
                </td>
              </tr>
              <tr className="bg-white border-b">
	            <td className="px-6 py-4">3</td>
                <td className="px-6 py-4">1234 5678 91011 1213</td>
                <td className="px-6 py-4">Perbaikan Fasilitas Umum</td>
                <td className="px-6 py-4 text-green-500">Diterima</td>
                <td className="px-6 py-4">Perbaikan</td>
                <td className="px-6 py-4">Cimahi</td>
                <td className="flex space-x-4 px-6 py-4">
                  <Accept/>
                  <Reject/>
                </td>
              </tr>
              <tr className="bg-white border-b">
	            <td className="px-6 py-4">4</td>
                <td className="px-6 py-4">1234 5678 91011 1213</td>
                <td className="px-6 py-4">Pelayanan Perawatan Gratis</td>
                <td className="px-6 py-4 text-red-500">Ditolak</td>
                <td className="px-6 py-4">Pelayanan</td>
                <td className="px-6 py-4">Cimahi</td>
                <td className="flex space-x-4 px-6 py-4">
                  <Accept/>
                  <Reject/>
                </td>
              </tr>
              <tr className="bg-white border-b">
	            <td className="px-6 py-4">5</td>
                <td className="px-6 py-4">1234 5678 91011 1213</td>
                <td className="px-6 py-4">Perbaikan Fasilitas Umum</td>
                <td className="px-6 py-4 text-green-500">Diterima</td>
                <td className="px-6 py-4">Perbaikan</td>
                <td className="px-6 py-4">Cimahi</td>
                <td className="flex space-x-4 px-6 py-4">
                  <Accept/>
                  <Reject/>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
	)
}