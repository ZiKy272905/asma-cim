import Download from '../atoms/ButtonDownload'

export default function ReportTable(){
	return(
        <div className="flex justify-center items-center w-full">
          <table className="w-full text-xs text-left text-gray-500 dark:text-gray-400">
            <thead className="text-sm text-gray-500 bg-gray-100">
              <tr>
                <th scope="col" className="px-6 py-3">
                  ID
                </th>
                <th scope="col" className="px-6 py-3">
                  Judul Laporan
                </th>
                <th scope="col" className="px-6 py-3">
                  NIK
                </th>
                <th scope="col" className="px-6 py-3">
                  
                </th>
              </tr>
            </thead>

            <tbody>
              <tr className="bg-white border-b">
	              <td className="px-6 py-4">1</td>
                <td className="px-6 py-4">Laporan Pengaduan</td>
                <td className="px-6 py-4">1234 5678 9012 1314</td>
                <td className="px-6 py-4">
                  <Download/>
                </td>
              </tr>
              <tr className="bg-white border-b">
                <td className="px-6 py-4">2</td>
                <td className="px-6 py-4">Laporan Aspirasi</td>
                <td className="px-6 py-4">1234 5678 9012 1314</td>
                <td className="px-6 py-4">
                  <Download/>
                </td>
              </tr>
              <tr className="bg-white border-b">
                <td className="px-6 py-4">3</td>
                <td className="px-6 py-4">Laporan Pengaduan</td>
                <td className="px-6 py-4">1234 5678 9012 1314</td>
                <td className="px-6 py-4">
                  <Download/>
                </td>
              </tr>
              <tr className="bg-white border-b">
                <td className="px-6 py-4">4</td>
                <td className="px-6 py-4">Laporan Aspirasi</td>
                <td className="px-6 py-4">1234 5678 9012 1314</td>
                <td className="px-6 py-4">
                  <Download/>
                </td>
              </tr>
              <tr className="bg-white border-b">
                <td className="px-6 py-4">5</td>
                <td className="px-6 py-4">Laporan Pengaduan</td>
                <td className="px-6 py-4">1234 5678 9012 1314</td>
                <td className="px-6 py-4">
                  <Download/>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
	)
}