import Head from 'next/head'

export default function Notification({
    show, 
    setShow, 
    title, 
    message, 
    icon, 
    no, 
    yes
  }) {

  const content = show && (
      <div className="flex justify-center items-center fixed md:inset-0 h-modal md:h-full bg-[#0085FF] bg-opacity-[7%] backdrop-blur-sm z-20">
        <div className="w-[35%] bg-white border rounded-xl shadow-lg">
          <div className="p-4 max-w-full max-h-full border-b">
            <h1 className=" text-lg font-semibold  tracking-tight text-black dark:text-white ">
              {title}
            </h1>
          </div>
          <div className="flex justify-center items-center w-full p-6">
            <div className="flex justify-center">
              {icon}
            </div>
            <div className="flex flex-col justify-center items-center w-full px-2">
              <p className="text-3xl font-semibold tracking-tight text-black">
                {message}
              </p>
            </div>
          </div>
          <div className="flex justify-center items-center w-full mb-6 space-x-10 px-20">
            <div className="flex justify-center items-center w-1/2">
              {no}
            </div>
            <div className="flex justify-center items-center w-1/2">
              {yes}
            </div>
          </div>
      </div>
    </div>
  );
  return content
};