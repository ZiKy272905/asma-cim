import Complaint from '../../children/officer/Complaint'
import Layout from './Layout'

export default function LayoutComplaint() {
	return(
		<Layout
			title="Pengaduan"
		>
			<Complaint/>
		</Layout>
	)
}