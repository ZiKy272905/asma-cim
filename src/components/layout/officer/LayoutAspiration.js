import Aspiration from '../../children/officer/Aspiration'
import Layout from './Layout'

export default function LayoutAspiration() {
	return(
		<Layout
			title="Aspirasi"
		>
			<Aspiration/>
		</Layout>
	)
}