import People from '../../children/officer/People'
import Layout from './Layout'

export default function LayoutPeople() {
	return(
		<Layout
			title="Masyarakat"
		>
			<People/>
		</Layout>
	)
}