import Response from '../../children/officer/Response'
import Layout from './Layout'

export default function LayoutResponse() {
	return(
		<Layout
			title="Tanggapan"
		>
			<Response/>
		</Layout>
	)
}