import Dashboard from '../../children/officer/Dashboard'
import Layout from './DashboardLayout'
import Card from '../../molecules/DashboardCard'
import Speech from '../../atoms/icons/Speech'
import Done from '../../atoms/icons/Done'
import Clock from '../../atoms/icons/Clock'

export default function LayoutDashboard() {
	return(
		<Layout
			waited={
				<Card
					title="Menunggu Tanggapan"
					icon={<Clock/>}
					fill="10"
				/>
			}
			responded={
				<Card
					title="Sudah Ditanggapi"
					icon={<Done/>}
					fill="20"
				/>
			}
			complaint={
				<Card
					title="Jumlah Pengaduan"
					icon={<Speech/>}
					fill="30"
				/>
			}
		>
			<Dashboard/>
		</Layout>
	)
}