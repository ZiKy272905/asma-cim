import Aspiration from '../../children/admin/Aspiration'
import Layout from './Layout'

export default function LayoutAspiration() {
	return(
		<Layout
			title="Aspirasi"
		>
			<Aspiration/>
		</Layout>
	)
}