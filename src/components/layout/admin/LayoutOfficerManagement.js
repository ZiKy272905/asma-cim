import Add from '../../atoms/ButtonCard'
import AddModal from '../../molecules/OfficerModal'
import OfficerManagement from '../../children/admin/OfficerManagement'
import Layout from './Layout'
import { useState } from 'react'

export default function LayoutOfficerManagement() {
	const [submitted, setSubmitted] = useState(false);
	const insertHandle = () => {
		setSubmitted(true)
	}
	return(
		<Layout
			title="Manajemen Petugas"
			button={
				<Add
					type="button"
					onClick={insertHandle}
				/>
			}
		>
			<OfficerManagement/>
			<AddModal
				setShow={setSubmitted}
				show={submitted}
			/>
		</Layout>
	)
}