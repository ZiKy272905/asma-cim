import Response from '../../children/admin/Response'
import Layout from './Layout'

export default function LayoutResponse() {
	return(
		<Layout
			title="Tanggapan"
		>
			<Response/>
		</Layout>
	)
}