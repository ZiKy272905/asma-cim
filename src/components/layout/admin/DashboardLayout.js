import Sidebar from '../../organisms/Sidebar'
import Navbar from '../../organisms/Navbar'
import Menu from '../../atoms/ButtonMenu'
import Dashboard from '../../atoms/ButtonDashboard'
import Link from 'next/link'
import Light from '../../atoms/icons/Light'
import File from '../../atoms/icons/File'
import Officer from '../../atoms/icons/Officer'
import Users from '../../atoms/icons/Users'
import Comment from '../../atoms/icons/Comment'
import Feedback from '../../atoms/icons/Feedback'

export function Layout({ 
    children,
    waited,
    responded,
    complaint
}) {
  return (
    <>
      {/* WRAPPER */}
      <div className="flex flex-row w-full h-full justify-center bg-gray-50">
        <div className="w-[20%] h-full bg-white">
          {/* WRAPPER SIDEBAR */}
          <div className="flex flex-col justify-start bg-white border-r box-border w-1/5 h-full fixed z-10 overflow-y-auto">
            <Sidebar
              Dashboard={
                <Dashboard
                  path="/admin/dashboard"
                />
              }
              MainMenus={[
                {MainMenuTitle:<Menu
                  type="button"
                  title="Aspirasi"
                  icon={<Light/>}
                  path="/admin/aspiration"
                />},
                {MainMenuTitle:<Menu
                  type="button"
                  title="Laporan"
                  icon={<File/>}
                  path="/admin/report"
                />},
                {MainMenuTitle:<Menu
                  type="button"
                  title="Manajemen Petugas"
                  icon={<Officer/>}
                  path="/admin/officer-management"
                />},
                {MainMenuTitle:<Menu
                  type="button"
                  title="Masyarakat"
                  icon={<Users/>}
                  path="/admin/people"
                />},
                {MainMenuTitle:<Menu
                  type="button"
                  title="Pengaduan"
                  icon={<Comment/>}
                  path="/admin/complaint"
                />},
                {MainMenuTitle:<Menu
                  type="button"
                  title="Tanggapan"
                  icon={<Feedback/>}
                  path="/admin/response"
                />}
              ]}
            />
          </div>
          {/* END OF WRAPPER SIDEBAR */}
        </div>
        <div className="w-[80%] h-full justify-center items-center">
          {/* WRAPPER CONTENT */}
          <div className="flex flex-col justify-start items-start w-full h-16">
            <div className="flex flex-col bg-white w-4/5 fixed z-10 border-b shadow-sm">
              <Navbar />
            </div>
          </div>
          <div className="flex flex-col justify-center items-center w-full p-6 space-y-4">
            <div className="flex justify-between items-center w-full space-x-4">
              <div className="flex justify-center items-center w-full rounded-md bg-white shadow-md">
                {responded}
              </div>
              <div className="flex justify-center items-center w-full rounded-md bg-white shadow-md">
                {waited}
              </div>
              <div className="flex justify-center items-center w-full rounded-md bg-white shadow-md">
                {complaint}
              </div>
            </div>

            <div className="flex justify-center items-center w-full rounded-md bg-white shadow-md p-4">
              {children}
            </div>
          </div>
          {/* END OF WRAPPER CONTENT */}
        </div>
      </div>
    </>
  );
}

export default Layout;