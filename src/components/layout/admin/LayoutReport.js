import Report from '../../children/admin/Report'
import Layout from './Layout'

export default function LayoutReport() {
	return(
		<Layout
			title="Laporan"
		>
			<Report/>
		</Layout>
	)
}