import Complaint from '../../children/admin/Complaint'
import Layout from './Layout'

export default function LayoutComplaint() {
	return(
		<Layout
			title="Pengaduan"
		>
			<Complaint/>
		</Layout>
	)
}