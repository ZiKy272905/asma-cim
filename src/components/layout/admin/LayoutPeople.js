import People from '../../children/admin/People'
import Layout from './Layout'

export default function LayoutPeople() {
	return(
		<Layout
			title="Masyarakat"
		>
			<People/>
		</Layout>
	)
}