import Pen from './icons/Pen'

export default function ButtonPen({
		type,
		onClick,
		icon
	}) {
	return(
		<button
			className="flex justify-center items-center"
			type={type}
			onClick={onClick}
		>
			<Pen/>
		</button>
	)
}