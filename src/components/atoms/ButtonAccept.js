import Accept from './icons/Accept'

export default function ButtonAccept({
		type,
		onClick,
		icon
	}) {
	return(
		<button
			className="flex justify-center items-center py-2"
			type={type}
			onClick={onClick}
		>
			<Accept/>
		</button>
	)
}