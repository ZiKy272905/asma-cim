import Link from 'next/link'

export default function ButtonMenu ({
		type, 
		onClick, 
		title, 
		path="", 
		icon
	}) {
	return(
		<Link href={path} className="flex justify-start items-center w-full">
			<button
				type={type}
				onClick={onClick} 
				className="text-sm text-left w-full rounded-md p-2 hover:bg-blue-500 hover:text-white hover:font-semibold"
			>
				<div className="flex justify-start items-center w-full space-x-2">
					{icon}
					<h1 className="font-normal">{title}</h1>
				</div>
			</button>
		</Link>
	)
}