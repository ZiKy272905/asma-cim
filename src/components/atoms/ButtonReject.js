import Reject from './icons/Reject'

export default function ButtonReject({
		type,
		onClick,
		icon
	}) {
	return(
		<button
			className="flex justify-center items-center py-2"
			type={type}
			onClick={onClick}
		>
			<Reject/>
		</button>
	)
}