import Download from './icons/Download'

export default function ButtonMenu ({
		type, 
		onClick, 
		title, 
		path="", 
		icon
	}) {
	return(
		<button
			type={type}
			onClick={onClick} 
			className="text-sm text-left w-full rounded-md p-2"
		>
			<div className="flex justify-start items-center w-full space-x-2">
				<Download/>
				<h1 className="font-normal">Download PDF</h1>
			</div>
		</button>
	)
}