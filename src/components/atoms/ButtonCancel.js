export default function ButtonCancel({ 
        type, 
        title,
        onClick
    }) {
    return (
        <button
            className= "border border-blue-700 text-blue-700 py-2 font-bold rounded-lg text-sm w-full"
            type= {type}
            onClick={onClick}
            >
            {title}
        </button>
    )
}