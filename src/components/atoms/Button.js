import Link from 'next/link'

export default function Button ({type, onClick, title, path=""}) {
	return(
		<Link href={path} className="flex justify-start items-center w-full">
			<button
				className="border border-blue-500 text-sm text-white text-center font-semibold w-full rounded-md p-2 bg-blue-500"
				type={type}
				onClick={onClick}
			>
				{title}
			</button>
		</Link>
	)
}