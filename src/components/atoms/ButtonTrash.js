import Trash from './icons/Trash'

export default function ButonTrash({
		type,
		onClick,
		icon
	}) {
	return(
		<button
			className="flex justify-center items-center"
			type={type}
			onClick={onClick}
		>
			<Trash/>
		</button>
	)
}