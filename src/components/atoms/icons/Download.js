import * as React from "react";

function Download(props) {
  return (
    <svg
      viewBox="0 0 24 24"
      fill="currentColor"
      height="1.5em"
      width="1.5em"
      {...props}
    >
      <path d="M19 9h-4V3H9v6H5l7 8zM4 19h16v2H4z" />
    </svg>
  );
}

export default Download;
