import Add from './icons/Add'

export default function ButtonCard({
		type,
		onClick,
		icon
	}) {
	return(
		<button
			className="flex justify-center items-center"
			type={type}
			onClick={onClick}
		>
			<Add/>
		</button>
	)
}