import Link from 'next/link'

export default function ButtonDashboard({
		path=""
	}){
	return(
		<Link 
			href={path} 
			className="flex justify-start items-center w-full">
			<button
				className="text-sm text-white text-left font-semibold w-full rounded-md p-2 bg-blue-500"
				type="button">
				Dashboard
			</button>
		</Link>
	)
}