export default function Input ({
		type, 
		placeholder, 
		id, 
		value, 
		onChange
	}) {
	return(
		<input
			className="w-full p-2 text-sm text-gray-900 rounded-md focus:outline-none border"
			type={type}
			placeholder={placeholder}
		>
      	</input>
	)
}