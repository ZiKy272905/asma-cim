export default function Label ({title}) {
	return(
		<label className="text-sm font-normal">
			{title}
      	</label>
	)
}