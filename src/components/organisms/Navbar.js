import User from '../atoms/icons/User'

export default function Navbar(){
	return(
		<div className="flex justify-center items-center p-4">
			<div className="flex justify-end items-center w-full">
				<button className="flex justify-start items-center space-x-2 mx-4">
	              <div className="flex justify-center items-center w-8 h-8 rounded-full bg-sky-100">
	                <User/>
	              </div>

	              <div className="flex justify-center">
	                <h1 className="text-black font-semibold">Admin - Administrasi</h1>
	              </div>
            </button>
			</div>
		</div>
	)
}