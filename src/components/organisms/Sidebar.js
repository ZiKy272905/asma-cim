import Link from 'next/link'
import Menu from '../atoms/ButtonMenu'
import Button from '../atoms/Button'
import Light from '../atoms/icons/Light'
import File from '../atoms/icons/File'
import Officer from '../atoms/icons/Officer'
import Users from '../atoms/icons/Users'
import Comment from '../atoms/icons/Comment'
import Feedback from '../atoms/icons/Feedback'

export default function Sidebar({
		MainMenus,
		MainMenu=[],
		MainMenuTitle,
		Dashboard
	}){
	return(
		<div className="flex flex-col justify-center items-center p-4">
			<div className="flex justify-center items-center w-full mb-8">
				<h1 className="text-xl font-bold tracking-widest">ASMA</h1>
			</div>

			<div className="flex flex-col justify-center w-full px-4 space-y-1 mb-8">
				<div className="flex justify-start items-center">
					<h1 className="text-sm font-semibold">BERANDA</h1>
				</div>
				<div className="flex justify-start items-center w-full">
					{Dashboard}
				</div>
			</div>

			<div className="flex flex-col justify-center w-full px-4 space-y-1 mb-8">
				<div className="flex justify-start items-center">
					<h1 className="text-sm font-semibold">MENU UTAMA</h1>
				</div>
				<div className="flex flex-col justify-center items-center w-full space-y-2">
					{MainMenus.map((MainMenu) => (
	        	  		{...MainMenu.MainMenuTitle}
	        	  	))}
				</div>
			</div>

		</div>
	)
}