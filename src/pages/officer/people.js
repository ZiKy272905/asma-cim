import Head from 'next/head'
import People from '../../components/layout/officer/LayoutPeople'

export default function PeoplePage() {
  return (
    <>
      <Head>
        <title>Petugas - Masyarakat</title>
        <meta name="description" content="Generated by create next app" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <People/>
    </>
  );
}